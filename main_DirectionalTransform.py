import numpy as np
import math
from PIL import Image

import matplotlib.pyplot as plt
import cDirectionalFilter 

# Import scipy for convolve2d
from scipy import signal as sg
import time

# Import pycuda for gpu convolution
import cuconv as cu
from scipy import misc

import sys

def Modulation(x):
	return np.abs(x)

# Parameters
level = 2
angular_delta = 1/9
delta = 1/9
length = 256
dt = 1.0
length_t = 21
FilterNum = 9
RotateStep = 20

cDirFilter = cDirectionalFilter.cDirectionalFilter(delta, length, length_t, dt, FilterNum, RotateStep)
cDirFilter.FilterList = cDirFilter.getDirectionalFilterList()

# Image Read
#Img = from_img('./red.png').astype(np.int32)
im = Image.open("./red.png")
#Transform to numpy array
Img = np.array(im, dtype=np.float32)
	
# Convolution using scipy
#start = time.time()
#for i in range(FilterNum-1):
#	FilteredImage[i, :, :] = sg.convolve2d(Img, cDirFilter.FilterList[i, :, :], "same")
#
#elapsed_time = time.time() - start
#print('Processing Time of Filter Convolution is ')
#print(elapsed_time)

#Convolution using pycuda
# GPU
start = time.time()

TensorList = []      #output TensorList
	
for iLayer in range(level):
	print("Layer : {0}".format(iLayer))
	Tensor = []
	AbsTensor = []
	for iChannel in range(max([FilterNum * iLayer, 1])):
		print("Channel : {0}".format(iChannel))
		if iLayer == 0:
			Input = Img
		else:
			#Input = TensorList[iLayer - 1][iChannel]
			Input = NextInput[iChannel]
		for iFilter in range(FilterNum):

			Filter = cDirFilter.FilterList[iFilter, :, :]
			Map = cu.convolve(Input, Filter)
	
			ModulatedMap = Modulation(Map)
			Tensor.append(ModulatedMap)
			#AbsTensor.append(np.abs(Map))
	TensorList.append(Tensor)
	#AbsTensorList.append(AbsTensor)
	NextInput = Tensor
	
end = time.time()
print("GPU time: %.5f s" % (end-start))

# Display
plt.figure
for i in range(FilterNum ** level):
	plt.subplot(1, 3, 1)
	plt.imshow(cDirFilter.FilterList[iFilter, :, :])
	plt.gray()
	print("Map {0}".format(i))
	plt.subplot(1, 3, 2)
	plt.imshow(TensorList[level - 1][i])
	plt.gray()
	plt.subplot(1, 3, 3)
	plt.imshow(Img)
	plt.gray()
	plt.show()
	print(np.sum(TensorList[level - 1][i]))









