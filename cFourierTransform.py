import numpy as np
import math
from PIL import Image
import matplotlib.pyplot as plt

class cFourierTransform():

    def __init__(self, length):
        self.omega = np.linspace(0.0, 2*np.pi, length)
        self.n = np.linspace(0.0, length - 1, length)
        self.n = self.n.reshape((length, -1))

        self.kernel_re = np.cos(self.omega * self.n)
        self.kernel_im = -1 * np.sin(self.omega * self.n)

        return

    def dft_1d(self, signal):
    
        F_im = np.dot(signal_x, self.kernel_im)
        F_re = np.dot(signal_x, self.kernel_re)
        
        return F_re, F_im

    def dft_2d(self, img):
        
        # 2d rr 
        F_rr = np.dot(img, self.kernel_re)
        F_rr = np.dot(F_rr.T, self.kernel_re)

        # 2d ri 
        F_ri = np.dot(img, self.kernel_re)
        F_ri = np.dot(F_ri.T, self.kernel_im)

        # 2d ir 
        F_ir = np.dot(img, self.kernel_im)
        F_ir = np.dot(F_ir.T, self.kernel_re)

        # 2d ii 
        F_ii = np.dot(img, self.kernel_im)
        F_ii = np.dot(F_ii.T, self.kernel_im)

        return F_rr, F_ri, F_ir, F_ii

    def ft_1d(self, signal, dt, length):

        _omega = np.linspace(-2*np.pi, 2*np.pi, spector.shape[0])
        t = np.linspace(-dt * math.floor(length/2), dt * math.floor(length/2), length)
        t = np.reshape((signal.shape[0], -1))

        _kernel_re = np.cos(_omega * t) * dt
        _kernel_im = -1 * np.sin(_omega * t) * dt

        F_im = np.dot(signal, _kernel_im)
        F_re = np.dot(signal, _kernel_re)
        
        return F_re, F_im

    def ift_1d(self, spector, _omega, dw, t, dt):
    
        _omega = _omega.reshape((_omega.shape[0], -1))
        _kernel_re = np.cos(_omega * t) * dw
        _kernel_im = np.sin(_omega * t) * dw
        #_kernel_im = np.sin(_omega * t - (dt/2)) * dw

        signal_re = np.dot(spector, _kernel_re)
        print(len(spector))
        for n in range(len(_omega)):
            if _omega[n] < 0:
                spector[n] = spector[n] * -1

        
        signal_im = np.dot(spector, _kernel_im)
        return signal_re, signal_im

    def ift_1d_HalfDelay(self, spector, _omega, dw, t, dt):
    
        # Half sample(dt / 2) delayed IFT to make Real Phi and Imag Phi
        _omega = _omega.reshape((_omega.shape[0], -1))
        _kernel_re = np.cos(_omega * t) * dw
        _kernel_im = np.cos(_omega * (t - dt/2)) * dw
        #_kernel_im = np.sin(_omega * t - (dt/2)) * dw

        signal_re = np.dot(spector, _kernel_re)
        #for n in range(len(_omega)):
        #    if _omega[n] < 0:
        #        spector[n] = spector[n] * -1

        signal_im = np.dot(spector, _kernel_im)
        return signal_re, signal_im



    def ift_2d(self, spector, _omega, dw, t, dt):

        #t = t.reshape((t.shape[0], -1))
        _omega = _omega.reshape((_omega.shape[0], -1))
    
        _kernel_re = np.cos(_omega * t) * dw
        _kernel_im = np.sin(_omega * t) * dw

    
        # 2d rr 
        rr = np.dot(spector, _kernel_re)
        rr = np.dot(rr.T, _kernel_re)
        rr = rr.T
        
        # 2d ri
        ri = np.dot(spector, _kernel_re)
        ri = np.dot(ri.T, _kernel_im)
        ri = ri.T
        
        # 2d ir 
        ir = np.dot(spector.T, _kernel_re)
        ir = ir.T
        ir = np.dot(ir, _kernel_im)
        
        # 2d ii 
        ii = np.dot(spector, _kernel_im)
        ii = np.dot(ii.T, _kernel_im)
        ii = ii.T
        
        return rr, ri, ir, ii

