import numpy as np
import math
from PIL import Image
import matplotlib.pyplot as plt
import cFourierTransform
import time

# for Phi convolution
from scipy import signal as sg

class cDirectionalFilter():

	def __init__(self, delta, length, length_t, dt, FilterNum, RotateStep):
		self.length = length
		self.delta = delta
		self.omega = np.linspace(-2*np.pi, 2*np.pi, length)
		self.length_t = length_t
		self.dt = dt
		self.FilterNum = FilterNum
		self.t = np.linspace(-self.dt * (self.length_t - 1)/2, self.dt*(self.length_t - 1)/2, self.length_t)
		self.FilterAngle = np.linspace(0, (self.FilterNum - 1)*(RotateStep), self.FilterNum)
		self.FilterList = np.zeros((self.FilterAngle.shape[0], self.t.shape[0], self.t.shape[0]), dtype="float64")
		self.FT = cFourierTransform.cFourierTransform(length)

		print('FilterList.shape is ')
		print(self.FilterList.shape)

		return

	def calc_nu(self, x):
		f = np.power(x, 4) * (35 - 84 * x + 70 * np.power(x, 2) - 20 * np.power(x, 3))
		return f

	def normalize(self, x):
		#mean = np.sum(x) / (x.shape[0] * x.shape[1])
		x_l2_norm = np.sum(x**2)**0.5
		return x / x_l2_norm
		#return ((x - mean) / np.sqrt(np.var(x)))

	def get_PhiList(self, shift):
	    #_omega = np.linspace(-2*np.pi, 2*np.pi, length)
	    _omega = self.omega
	    dw = 4*np.pi / self.length
	    Phi = np.zeros((_omega.shape))
	    Phi = np.cos(np.pi / 2 * self.calc_nu((1 - self.delta) / (2 * self.delta)*(np.abs(_omega - shift) / ((1 - self.delta)*np.pi) - 1)))

	    Phi[math.floor(self.length/2):self.length] = Phi[0:math.floor(self.length/2)]

	    ind = math.floor((2*np.pi - (1 + self.delta)*np.pi - shift) / dw)
	    
	    Phi[0:ind] = 0.0
	    Phi[self.length - ind:Phi.shape[0]] = 0.0

	    ind_start = math.floor((2*np.pi - (1 - self.delta) * np.pi) / dw)
	    ind_end = self.length - ind_start

	    Phi[ind_start:ind_end] = 1.0
	    return Phi

	def getPhiValue(self, radious, shift):

		#print(radious, (1 - delta - shift) * np.pi, (1 + delta - shift) * np.pi)
		if radious < (1 - self.delta - shift) * np.pi:
			return 1.0
		elif radious > (1 + self.delta - shift) * np.pi:
			return 0.0
		else:
			output = np.cos(np.pi / 2 * self.calc_nu((1 - self.delta) / (2 * self.delta)*(np.abs(radious +  shift*np.pi) / ((1 - self.delta)*np.pi) - 1)))
			return output

	def getAngularFuncValue(self, center, theta):

		output = 0.0
		left = center - (2*self.delta*np.pi)
		right = center + (2*self.delta*np.pi)

		if right > np.pi and theta < right - np.pi:
			theta = theta + 2*np.pi

		if theta > right:
			output = 0.0

		elif theta < left:
			output = 0.0

		elif left <= theta and theta <= center: # left
			output = np.cos(np.pi / 2 * self.calc_nu((1 - self.delta) / (2 * self.delta)*(np.abs(theta - (center - ((self.delta - 1)*np.pi))) / ((1 - self.delta)*np.pi) - 1)))

		elif center <= theta and theta <= right: # right
			output = np.cos(np.pi / 2 * self.calc_nu((1 - self.delta) / (2 * self.delta)*(np.abs(theta + ((1 - self.delta)*np.pi - center)) / ((1 - self.delta)*np.pi) - 1)))

		else:
			output = 0.0

		return output

	def getDirectionalFilterList(self):

		omega_x = self.omega
		omega_y = self.omega
		dw = omega_x[1] - omega_x[0]
		length = self.length

		SmallCircleMask = np.zeros((length, length))
		DirectionalMask= np.zeros((length, length))
		CircleMask = np.zeros((length, length))
		
		shift = 0.5
		start = time.time()
		for iy in range(length):
			for ix in range(length):
				radious = np.sqrt(omega_y[iy] * omega_y[iy] + omega_x[ix] * omega_x[ix])
				CircleMask[iy][ix] = self.getPhiValue(radious, 0.0)
				SmallCircleMask[iy][ix] = self.getPhiValue(radious, shift)
		
		for n in range(self.FilterAngle.shape[0]):
			print(self.FilterAngle[n])
			for iy in range(length):
				for ix in range(length):
					radious = np.sqrt(omega_y[iy] * omega_y[iy] + omega_x[ix] * omega_x[ix])
					DirectionalMask[iy][ix] = self.getAngularFuncValue(self.FilterAngle[n] * np.pi / 180.0, np.arctan2(iy - length/2 , ix - length/2))

			DirectionalMask= DirectionalMask* (CircleMask - SmallCircleMask)
			DirectionalMask_flip = np.flip(DirectionalMask, axis = 0)
			DirectionalMask_flip = np.flip(DirectionalMask_flip, axis = 1)

			DirectionalMask= DirectionalMask + DirectionalMask_flip
			#plt.figure()
			#plt.imshow(DirectionalMask)
			#plt.show()

			#plt.figure()
			#plt.plot(DirectionalMask[math.floor(length/2), :])
			#plt.show()


			rr, _, _, ii = self.FT.ift_2d(DirectionalMask, omega_x, dw, self.t, self.dt)
			self.FilterList[n,:,:] = self.normalize(rr + ii)
			#self.FilterList[n,:,:] = (rr + ii)

			
		elapsed_time = time.time() - start
		print('Processing Time of Generate filter is ')
		print(elapsed_time)

		return self.FilterList