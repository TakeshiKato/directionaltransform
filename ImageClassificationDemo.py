import numpy as np
import math
from PIL import Image
import matplotlib.pyplot as plt
import cDirectionalFilter 

# Import scipy for convolve2d
from scipy import signal as sg
import time

# Import pycuda for gpu convolution
#import cuconv as cu
from scipy import misc

# For NN
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# To Ready directional filter
delta = 1/9
length = 128
dt = 1
length_t = 15
FilterNum = 9
RotateStep = 20
cDirFilter = cDirectionalFilter.cDirectionalFilter(delta, length, length_t, dt, FilterNum, RotateStep)
cDirFilter.FilterList = cDirFilter.getDirectionalFilterList()

def DirectionalTransform(FilterList, Image, Padding):
    #PaddingLength = math.floor(FilterList.shape[1] / 2)
    FilteredImage = np.zeros((Image.shape[2] * FilterList.shape[0], Image.shape[0], Image.shape[1]))
    for ch in range(Image.shape[2]):
        for i in range(FilterNum-1):
            TargetImage = Image[:,:,ch]
            #if Padding == 1:
            #    TargetImage = np.pad(TargetImage, (PaddingLength, PaddingLength), 'constant')

            #FilteredImage[(Image.shape[2] * ch) + i, :, :] = np.asarray(cu.convolve(TargetImage, FilterList[i, :, :]))
            FilteredImage[(Image.shape[2] * ch) + i, :, :] = sg.convolve2d(TargetImage, FilterList[i, :, :], "same")
            #FilteredImage = np.asarray(cu.convolve(TargetImage, FilterList[i, :, :]))

    return FilteredImage


#check GPU is available
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# Load CIFAR10 Data
transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)

testset=torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
testloader=torch.utils.data.DataLoader(testset, batch_size = 4, shuffle=False, num_workers=2)

classes=('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

# define net
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        #self.conv1 = nn.Conv2d(27, 6, 5)
        #self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2,2)
        #self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(27*16*16, 1200)
        self.fc2 = nn.Linear(1200, 120)
        self.fc3 = nn.Linear(120, 10)

    def forward(self, x):
        #x = self.pool(F.relu(self.conv1(x)))
        #x = self.pool(F.relu(self.conv2(x)))
        x = self.pool(x)
        x = x.view(-1, 27*16*16)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()
net.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

for epoch in range(5):
    running_loss = 0.0
    
    for i, data in enumerate(trainloader, 0):
        #inputs, labels = data # for cpu
        inputs, labels = data[0].to(device), data[1].to(device) # for gpu
        optimizer.zero_grad()

        #[Begin] directional transform process

        #print("inputs tensor device:", data[0].device)
        #print("inputs tensor dtype:", data[0].dtype)
        BatchImage = data[0].numpy()
        
        FilteredTensor = torch.zeros(data[0].size(0), data[0].size(1) * cDirFilter.FilterList.shape[0], data[0].size(2), data[0].size(3))
        
        for iBatch in range(data[0].size(0)): #Batch Loop
            Image = BatchImage[iBatch, :, :, :]
            Image = Image / 2 + 0.5
            Image = np.transpose(Image, (1,2,0))
            FilteredImage = DirectionalTransform(cDirFilter.FilterList, Image, 1)
            FilteredTensor[iBatch, :,:,:] = torch.from_numpy(FilteredImage)

        
        Input = FilteredTensor.to(device)
        
        #[End] directional transform process

        #outputs = net(inputs)
        outputs = net(Input)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
                
        running_loss += loss.item()
        if i % 1000 == 999:
            print('[%d, %5d] loss : %.3f' %(epoch + 1, i+1, running_loss / 2000))
            running_loss = 0.0

print('Finished Training')

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))

with torch.no_grad():
    for data in testloader:
        images, labels = data[0].to(device), data[1].to(device)
        #inputs, labels = data[0].to(device), data[1].to(device)
        outputs = net(images)
        _, predicted = torch.max(outputs.data, 1)
        c = (predicted == labels).squeeze()
        for i in range(4):
            label = labels[i]
            class_correct[label] += c[i].item()
            class_total[label] += 1

for i in range(10):
    print('Accuracy of %5s : %2d %%' % (classes[i], 100 * class_correct[i] / class_total[i]))
